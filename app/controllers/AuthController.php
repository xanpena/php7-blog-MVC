<?php

namespace BlogMVC\Controllers;

use BlogMVC\Log;
use BlogMVC\Controllers\BaseController;
use BlogMVC\Models\User;
use Sirius\Validation\Validator;

class AuthController extends BaseController {
    
    public function getLogin(){ 
        return $this->render('login.twig');
    }

    public function postLogin() {
        $validator = new Validator();
        $validator->add('email', 'required | email');
        $validator->add('password', 'required');
        
        if ($validator->validate($_POST)) {
            $user = User::where('email', $_POST['email'])->first();
            if ($user) {
                if (password_verify($_POST['password'], $user->password)) {
                    $_SESSION['user_id'] = $user->id;
                    Log::logInfo('Login userId:' . $user->id);
                    header('Location:'.BASE_URL.'admin');
                    return null;
                }
            }
            $validator->addMessage('email', 'Log in fail');
        }

        $errors = $validator->getMessages();
        return $this->render('login.twig', [
            'errors' => $errors
        ]);
    }


    public function getLogout() {
        Log::logError('Logout userId:' . $_SESSION['user_id']);
        unset($_SESSION['user_id']);
        header('Location: ' . BASE_URL . 'auth/login');
    }

}