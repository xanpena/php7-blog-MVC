<?php

namespace BlogMVC\Controllers;

use BlogMVC\Models\User;

class BackController extends BaseController {
    
    public function getIndex(){
        
        if(isset($_SESSION['user_id'])){
            $user_id = $_SESSION['user_id'];
            $user = User::find($user_id);

            if($user){
                return $this->render('admin/index.twig', ['user'=>$user]);
            }
        }

        header('Location: '.BASE_URL.'auth/login');
    }
}