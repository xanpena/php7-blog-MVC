<?php

namespace BlogMVC\Controllers;

use BlogMVC\Controllers\BaseController;
use BlogMVC\Models\User;
use Sirius\Validation\Validator;

class UserController extends BaseController {
    
    public function getIndex(){
        $users = User::all();
        return $this->render('admin/users.twig', ['users' => $users]);
    }

    public function getNew(){
        return $this->render('admin/insert-user.twig');
    }

    public function postNew(){ 
        $errors=[];
        $result = false;

        $validator = new \Sirius\Validation\Validator();
        $validator->add('name:Name', 'required');
        $validator->add('email:Email', 'email | required');
        $validator->add('password:Password', 'required');

        if($validator->validate($_POST)){
            $user = new User();
            $user->name = $_POST['name'];
            $user->email = $_POST['email'];
            $user->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $user->save();
            $result = true;
        }else{
            $errors = $validator->getMessages();
        }

        return $this->render('admin/insert-user.twig', ['result' => $result, 'errors' => $errors]);
    }
}