<?php

namespace BlogMVC\Controllers;

use BlogMVC\Controllers\BaseController;
use BlogMVC\Models\BlogPost;
use Sirius\Validation\Validator;

class PostController extends BaseController {
    
    public function getIndex(){
        $blogPosts = BlogPost::all();
        return $this->render('admin/posts.twig', ['blogPosts' => $blogPosts]);
    }

    public function getNew(){
        return $this->render('admin/insert-post.twig');
    }

    public function postNew(){ 
        $errors=[];
        $result = false;

        $validator = new \Sirius\Validation\Validator();
        $validator->add('title:Title', 'required');
        $validator->add('content:Content', 'required');

        if($validator->validate($_POST)){
            
            $blogPost = new BlogPost([
                'title' => $_POST['title'],
                'content' => $_POST['content']
            ]);

            if($_POST['image']){
                $blogPost->image = $_POST['image'];
            }
    
           // $blogPost->save();
            $result = true;
        }else{
            $errors = $validator->getMessages();
        }

        return $this->render('admin/insert-post.twig', ['result' => $result, 'errors' => $errors]);
    }
}