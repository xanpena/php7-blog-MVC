<?php

namespace BlogMVC\Controllers;

use BlogMVC\Models\BlogPost;

class FrontController extends BaseController {
    
    public function getIndex(){ 
        $blogPosts = BlogPost::all();
        return $this->render('index.twig', ['blogPosts' => $blogPosts]);
    }
}