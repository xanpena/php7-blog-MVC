<?php

ini_set('display_errors', 1);
ini_set('dispay_startup_errors', 1);
error_reporting(E_ALL);

require_once '../vendor/autoload.php';

session_start();

$baseDir = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$baseDir;
define('BASE_URL', $baseUrl);

$dotenv = new \Dotenv\Dotenv(__DIR__ . '/..');
$dotenv->load();

/***** Eloquent ORM *****/
use Illuminate\Database\Capsule\Manager as Capsule;
$capsule = new Capsule; 
$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => getenv('DB_HOST'),
    'database'  => getenv('DB_NAME'),
    'username'  => getenv('DB_USER'),
    'password'  => getenv('DB_PASS'),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);
// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();
// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

/***** Fin Eloquent ORM *****/

// Operador NULL para verificar si existe nuestro parámetro, si no entendemos que estamos en la raíz del sitio
//$route = $_GET['route'] ?? '/'; 
$route = isset($_GET['route']) ? $_GET['route'] : '/';

use Phroute\Phroute\RouteCollector;

$router = new RouteCollector();

$router->filter('auth', function(){
    if(!isset($_SESSION['user_id'])){
        header('Location: '.BASE_URL.'auth/login');
        return false;
    }
});

$router->controller('/', BlogMVC\Controllers\FrontController::class);
$router->controller('/auth', BlogMVC\Controllers\AuthController::class);

$router->group(['before' => 'auth'], function($router){
    $router->controller('/admin', BlogMVC\Controllers\BackController::class);
    $router->controller('/admin/posts', BlogMVC\Controllers\PostController::class);
    $router->controller('/admin/users', BlogMVC\Controllers\UserController::class);
});


$dispatcher = new Phroute\Phroute\Dispatcher($router->getData());
$response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], $route);
echo $response;
